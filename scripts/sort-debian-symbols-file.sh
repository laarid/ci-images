#!/bin/sh

export LC_ALL=C

test -r "$1" || (echo "failed to open symbol file $1: error $?" >&2; exit 1)

O=$(cat "$1" | c++filt \
    | sed '/^ /{ s/^ \(([^)]\+)"\?\)\?\(.*\)\("\? [0-9\.]\+\)$/\2@@ \1\2\3/}')
echo "$O" | grep --invert-match @@;
echo "$O" | grep @@ \
    | sort --field-separator=@ --key=1 --stable --unique --ignore-case \
    | sed 's/^.*@@//'
